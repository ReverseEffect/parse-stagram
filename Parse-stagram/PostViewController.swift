//
//  PostViewController.swift
//  Parse-stagram
//
//  Created by Robert on 1/5/17.
//  Copyright © 2017 ReverseEffect Applications. All rights reserved.
//

import UIKit
import Parse

class PostViewController: UIViewController, UINavigationControllerDelegate, UIImagePickerControllerDelegate {

    @IBOutlet weak var previewImageView: UIImageView!
    @IBOutlet weak var captionTextField: UITextField!
    var isImageChosen = false
    var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationItem.title = "Create Post"
        
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = .gray
        view.addSubview(activityIndicator)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showConfirmAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Yes", style: .destructive, handler: {
            (action) -> Void in
            
            self.dismiss(animated: true, completion: {
                () -> Void in
                
                self.dismiss(animated: true, completion: nil)
            })
        })
        let cancelAction = UIAlertAction(title: "No", style: .default, handler: nil)
        alertController.addAction(alertAction)
        alertController.addAction(cancelAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showDismissAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Ok", style: .default, handler: {
            (action) -> Void in
            
            self.dismiss(animated: true, completion: {
                () -> Void in
                
                self.dismiss(animated: true, completion: nil)
            })
        })
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func buttonPressed(sender: UIView) {
        if sender.tag == 101 {
            let imagePicker = UIImagePickerController()
            imagePicker.delegate = self
            imagePicker.sourceType = UIImagePickerControllerSourceType.photoLibrary
            imagePicker.allowsEditing = false
            
            present(imagePicker, animated: true, completion: nil)
        }
        else if sender.tag == 102 {
            activityIndicator.startAnimating()
            UIApplication.shared.beginIgnoringInteractionEvents()
            
            if !isImageChosen || captionTextField.text == "" {
                var message: String?
                
                if !isImageChosen && captionTextField.text == "" {
                    message = "Please select an image and write a short caption."
                }
                else if !isImageChosen {
                    message = "Please select an image."
                }
                else if captionTextField.text == "" {
                    message = "Please write a short caption."
                }
                
                showAlert(title: "Unable to Post", message: message!)
            }
            else {
                let post = PFObject(className: "Posts")
                post["message"] = captionTextField.text
                post["userId"] = PFUser.current()!.objectId!
                
                let rng = arc4random_uniform(UInt32(999999999))
                
                let imageData = UIImageJPEGRepresentation(previewImageView.image!, CGFloat(0.8))
                let imageFile = PFFile(name: "\(PFUser.current()!.objectId!)_\(rng).jpg", data: imageData!)
                
                post["imageFile"] = imageFile
                
                post.saveInBackground(block: {
                    (success, error) in
                    
                    UIApplication.shared.endIgnoringInteractionEvents()
                    self.activityIndicator.stopAnimating()
                    
                    if error != nil {
                        self.showAlert(title: "Couldn't post image", message: "\(error!.localizedDescription)")
                    }
                    else {
                        self.showDismissAlert(title: "Post Success", message: "We posted your image to your profile!")
                    }
                })
            }
        }
        else if sender.tag == 103 {
            showConfirmAlert(title: "Cancel post?", message: "Are you sure you wish to cancel your post?")
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            previewImageView.image = image
        }
        
        dismiss(animated: true, completion: {
            self.isImageChosen = true
        })
    }
}
