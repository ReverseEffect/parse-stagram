//
//  LightContentNavController.swift
//  Parse-stagram
//
//  Created by Robert on 1/5/17.
//  Copyright © 2017 ReverseEffect Applications. All rights reserved.
//

import UIKit

class LightContentNavController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.navigationBar.titleTextAttributes = [NSForegroundColorAttributeName: UIColor.white,
                                                  NSFontAttributeName: UIFont(name: "AvenirNext-Regular", size: 20)!]
        self.navigationBar.tintColor = UIColor.white
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
