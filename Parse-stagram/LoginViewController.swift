//
//  LoginViewController.swift
//  Parse-stagram
//
//  Created by Robert on 1/4/17.
//  Copyright © 2017 ReverseEffect Applications. All rights reserved.
//

import UIKit
import Parse

class LoginViewController: UIViewController {

    @IBOutlet weak var loginContainerView: UIView!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var keyboardToolbar: UIToolbar!
    var activityIndicator: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginContainerView.layer.borderWidth = 1
        loginContainerView.layer.borderColor = UIColor.black.cgColor
        
        keyboardToolbar = UIToolbar(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: 44))
        let space = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        let dismissKeyboardButton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(LoginViewController.dismissKeyboard))
        keyboardToolbar.items = [space, dismissKeyboardButton]
        usernameTextField.inputAccessoryView = keyboardToolbar
        passwordTextField.inputAccessoryView = keyboardToolbar
    }
    
    override func viewDidAppear(_ animated: Bool) {
        activityIndicator = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = .gray
        self.view.addSubview(activityIndicator)
        
        UIView.setAnimationsEnabled(true) // At this point, any future present/dismiss segues can be animated.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func loginButtonPressed(sender: UIButton) {
        if usernameTextField.text == "" || passwordTextField.text == "" {
            showAlert(title: "Unable to Login/Register", message: "Please fill out all fields and try again.")
        }
        else {
            if sender.tag == 101 {
                // Login
                PFUser.logInWithUsername(inBackground: usernameTextField.text!, password: passwordTextField.text!, block: {
                    (user, error) in
                    
                    if error != nil {
                        self.showAlert(title: "Sign In Error", message: "\(error!.localizedDescription)")
                    }
                    else {
                        self.showConfirmAlert(title: "Signed In", message: "You have signed in as \(self.usernameTextField.text!)!")
                    }
                })
            }
            else if sender.tag == 102 {
                activityIndicator.startAnimating()
                UIApplication.shared.beginIgnoringInteractionEvents()
                
                let user = PFUser()
                user.username = usernameTextField.text
                user.password = passwordTextField.text
                
                user.signUpInBackground(block: {
                    (success, error) in
                    
                    self.activityIndicator.stopAnimating()
                    UIApplication.shared.endIgnoringInteractionEvents()
                    
                    if error != nil {
                        self.showAlert(title: "Register Error", message: "\(error!.localizedDescription)")
                    }
                    else {
                        self.showConfirmAlert(title: "Register", message: "You are now signed up for Parsestagram!")
                    }
                })
            }
        }
    }
    
    func dismissKeyboard() {
        if usernameTextField.isFirstResponder {
            usernameTextField.resignFirstResponder()
        }
        else if passwordTextField.isFirstResponder {
            passwordTextField.resignFirstResponder()
        }
    }
    
    func showAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Dismiss", style: .default, handler: nil)
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func showConfirmAlert(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: "Dismiss", style: .default, handler: {
            (action) -> Void in
            
            self.dismiss(animated: true, completion: {
                () -> Void in
                
                self.dismiss(animated: true, completion: nil)
            })
        })
        alertController.addAction(alertAction)
        present(alertController, animated: true, completion: nil)
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}
