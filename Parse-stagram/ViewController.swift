//
//  ViewController.swift
//  Parse-stagram
//
//  Created by Robert on 1/4/17.
//  Copyright © 2017 ReverseEffect Applications. All rights reserved.
//

import UIKit
import Parse

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    var tableArray = [String]()
    var idArray = [String]()
    var usersFollowed = [String: Bool]()
    var refreshControl: UIRefreshControl!
    
    var REUSE_IDENTIFIER = "reuseIdentifier"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = "Parsestagram"
        
        UIView.setAnimationsEnabled(false) // Disable first segue animation for good user experience. Ugly if first transition is animated.
        if PFUser.current() == nil {
            performSegue(withIdentifier: "signInSegue", sender: nil)
        }
        else {
            print("Current User: \(PFUser.current()!.username!)")
        }
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(ViewController.refresh), for: UIControlEvents.valueChanged)
        tableView.addSubview(refreshControl)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        refresh()
    }
    
    func refresh() {
        if PFUser.current() != nil {
            let query = PFUser.query()
            query!.whereKey("username", notEqualTo: PFUser.current()!.username!)
            query?.findObjectsInBackground(block: {
                (objects, error) -> Void in
                
                self.tableArray = [String]()
                self.idArray = [String]()
                self.usersFollowed = [String: Bool]()
                
                if error != nil {
                    print("\(error!.localizedDescription)")
                }
                else {
                    for currentUser in objects as! [PFUser] {
                        self.tableArray.append("\(currentUser.value(forKey: "username")!)")
                        self.idArray.append("\(currentUser.value(forKey: "objectId")!)")
                        
                        let query = PFQuery(className: "Followers")
                        query.whereKey("follower", equalTo: PFUser.current()?.objectId as String!)
                        query.whereKey("following", equalTo: currentUser.objectId!)
                        
                        query.findObjectsInBackground(block: {
                            (objects, error) -> Void in
                            
                            if error != nil {
                                print("\(error!.localizedDescription)")
                            }
                            else {
                                if let objects = objects as [PFObject]! {
                                    if objects.count > 0 {
                                        self.usersFollowed[currentUser.objectId!] = true
                                    }
                                    else {
                                        self.usersFollowed[currentUser.objectId!] = false
                                    }
                                    
                                    if self.usersFollowed.count == self.tableArray.count {
                                        self.tableView.reloadData()
                                        self.refreshControl.endRefreshing()
                                    }
                                }
                            }
                        })
                    }
                }
                
                self.tableView.reloadData()
            })
        }
        else {
            print("PFUser.current() is nil. Skip loading data source")
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "signInSegue" {
            if PFUser.current() != nil {
                let currentUsername = PFUser.current()!.username
                
                let query = PFSession.query()
                query!.findObjectsInBackground(block: {
                    (objects, error) in
                    
                    if error != nil {
                        print("\(error!.localizedDescription)")
                    }
                    else {
                        // Purge extra sessions if necessary
                        for session in objects as [PFObject]! {
                            if let session = session as PFObject! {
                                let sessionUser = session.value(forKey: "user") as! PFUser
                                let sessionUsername = sessionUser.username
                                
                                if currentUsername == sessionUsername {
                                    session.deleteInBackground()
                                }
                            }
                        }
                    }
                })
                
                PFUser.logOut()
            }
            else {
                print("PFUser.current() is nil")
            }
        }
        else if segue.identifier == "postSegue" || segue.identifier == "feedSegue" {
            UIView.setAnimationsEnabled(true) // It's possible the user may already be logged in and this be false.
        }
    }
    
    @IBAction func signOut(_ sender: UIBarButtonItem) {
        tableArray.removeAll()
        idArray.removeAll()
        usersFollowed.removeAll()
        tableView.reloadData()
        
        UIView.setAnimationsEnabled(true) // It's ok to enable segue animations here as user is logging out.
        performSegue(withIdentifier: "signInSegue", sender: sender)
    }
    
    // MARK: - UITableView delegate and data source
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tableArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: REUSE_IDENTIFIER)
        cell!.textLabel!.text = tableArray[indexPath.row]
        
        if usersFollowed.count != 0 {
            if usersFollowed[idArray[indexPath.row]] != nil {
                if usersFollowed[idArray[indexPath.row]]! {
                    cell!.accessoryType = .checkmark
                }
                else {
                    cell!.accessoryType = .none
                }
            }
        }
        
        cell!.selectionStyle = .default
        
        print("Users Followed: \(usersFollowed.count)")
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
        
        let cell = tableView.cellForRow(at: indexPath)! as UITableViewCell
        
        if cell.accessoryType == .checkmark {
            cell.accessoryType = UITableViewCellAccessoryType.none

            let query = PFQuery(className: "Followers")
            query.whereKey("follower", equalTo: PFUser.current()?.objectId as String!)
            query.whereKey("following", equalTo: idArray[indexPath.row])
            
            query.findObjectsInBackground(block: {
                (objects, error) -> Void in
                
                for currentObject in objects as [PFObject]! {
                    currentObject.deleteInBackground()
                }
            })
        }
        else if cell.accessoryType == .none {
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
            
            let following = PFObject(className: "Followers")
            following["follower"] = PFUser.current()?.objectId!
            following["following"] = idArray[indexPath.row]
            following.saveInBackground()
        }
    }
}

